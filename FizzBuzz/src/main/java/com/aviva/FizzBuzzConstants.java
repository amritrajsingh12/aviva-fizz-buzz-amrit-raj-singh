package main.java.com.aviva;

public final class FizzBuzzConstants {

	private FizzBuzzConstants() {
        throw new Error();
    }
	
	public static final Integer Three = 3;
	public static final Integer FIVE = 5;
	public static final String FIZZ = "FIZZ";
	public static final String BUZZ = "BUZZ";
	public static final String FIZZ_BUZZ = "FIZZBUZZ";
	public static final String WIZZ = "WIZZ";
	public static final String WUZZ = "WUZZ";
}
