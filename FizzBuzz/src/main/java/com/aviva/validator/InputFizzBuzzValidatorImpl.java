package main.java.com.aviva.validator;

import main.java.com.aviva.validator.interfaces.IInputValidator;

public class InputFizzBuzzValidatorImpl implements IInputValidator {

	public boolean isValidInput(String input) {
		Integer inputNumber=Integer.parseInt(input);
		if(inputNumber<0){
			return false;
		}
		return true;
	}

	
	public boolean isInputInValidRange(String input) {
		Integer inputNumber=Integer.parseInt(input);
		
		if(inputNumber < 0 || inputNumber>1000){
			return false;
		}
		return true;
	}
}
