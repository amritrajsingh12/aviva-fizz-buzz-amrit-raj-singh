package main.java.com.aviva.validator.interfaces;

public interface IInputValidator {

	boolean isValidInput(String string);

	boolean isInputInValidRange(String string);

}
