package main.java.com.aviva.controller;

import main.java.com.aviva.helper.FizzBuzzHelper;
import main.java.com.aviva.validator.InputFizzBuzzValidatorImpl;
import main.java.com.aviva.validator.interfaces.IInputValidator;


/**
 * 
 * 
 * This class is responsible for fiz buzz implementation end to end
 * ||
 * ||
 * FizzBuzzController 
 * 					|| 
 * 					|| 
 * 					InputValidator 
 * 								|| 
 * 								|| 
 * 								FizzBuzzHelper
 * 
 * @author Amrit
 * 
 */
public class FizzBuzzController {
	
	private static IInputValidator inputValidator;

	private static FizzBuzzHelper fizzBuzzHelper;

	public static FizzBuzzHelper getiFizBuzzHelper() {
		if (fizzBuzzHelper == null) {
			fizzBuzzHelper = new FizzBuzzHelper();
		}
		return fizzBuzzHelper;
	}

	public static void setiFizBuzzHelper(FizzBuzzHelper fizzBuzzHelper) {
		FizzBuzzController.fizzBuzzHelper = fizzBuzzHelper;
	}

	public static IInputValidator getInputValidator() {
		if (inputValidator == null) {
			inputValidator = new InputFizzBuzzValidatorImpl();
		}
		return inputValidator;
	}

	public void setInputValidator(IInputValidator inputValidator) {

		this.inputValidator = inputValidator;
	}
	
	public static void main(String[] args) {

		String inputString = args[0];
		
		try {
			inputValidator = getInputValidator();
			boolean isValidInput = inputValidator.isValidInput(inputString);
			if (!isValidInput) {
				System.out
						.println("Please enter number between 1 and 1000 .current user input is "
								+ inputString);
			} else {
				fizzBuzzHelper = getiFizBuzzHelper();

				boolean isSuccessfull = fizzBuzzHelper.printFizBuzz(inputString);

			}
		} catch (Exception e) {
			System.out.println("System Error");
		}
	}
}
