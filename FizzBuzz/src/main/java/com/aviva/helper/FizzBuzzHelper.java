package main.java.com.aviva.helper;

import main.java.com.aviva.FizzBuzzConstants;
import main.java.com.aviva.util.FizzBuzzUtil;

public class FizzBuzzHelper {

	FizzBuzzUtil util= new FizzBuzzUtil();
	
	public boolean printFizBuzz(String inputString) {
		Integer inputNumber= Integer.parseInt(inputString);
		
		for(int i=2;i<inputNumber;i++){
			String printWord=getPrintWord(i);
			if(printWord !=null){
					System.out.println(printWord+"/n");
			}
		}
		return false;
	}

	public String getPrintWord(int i) {

		if(util.isZeroRemainder(i,FizzBuzzConstants.Three) && util.isZeroRemainder(i,FizzBuzzConstants.FIVE)){
			return FizzBuzzConstants.FIZZ_BUZZ; 
		}else if(util.isZeroRemainder(i,FizzBuzzConstants.Three)){
			if(!util.isWedenesday()){
				return FizzBuzzConstants.FIZZ;
			}else{
				return FizzBuzzConstants.WIZZ;
			}
		}else if(util.isZeroRemainder(i,FizzBuzzConstants.FIVE)){
			if(!util.isWedenesday()){
				return FizzBuzzConstants.BUZZ;
			}else{
				return FizzBuzzConstants.WUZZ;
			}
		}
		return null;
	}

}
