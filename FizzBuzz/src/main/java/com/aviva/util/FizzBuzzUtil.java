package main.java.com.aviva.util;

import java.util.Calendar;

public class FizzBuzzUtil {
	
	public boolean isZeroRemainder(int dividend, int divisor) {
		if(dividend % divisor == 0){
			return true;
		}
		return false;
	}
	
	
	public boolean isWedenesday(){
		Calendar startDate = Calendar.getInstance();
		if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
			return true;
		}
		return false;
	}
}
