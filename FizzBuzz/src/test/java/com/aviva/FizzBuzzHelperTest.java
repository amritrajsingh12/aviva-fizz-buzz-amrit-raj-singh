package test.java.com.aviva;

import static org.junit.Assert.*;

import org.junit.Test;

import main.java.com.aviva.helper.FizzBuzzHelper;


public class FizzBuzzHelperTest {
	
	FizzBuzzHelper helper=new FizzBuzzHelper();
	
	@Test
	   public void returnsFizzWhenNumberIsMultipleOfThree() throws Exception {
	       assertEquals("fizz", helper.getPrintWord(3));
	       assertEquals("fizz", helper.getPrintWord(6));
	       assertEquals("fizz", helper.getPrintWord(9));
	  }
	
	@Test
	   public void returnsBuzzWhenNumberIsMultipleOfFive() throws Exception {
	       assertEquals("buzz", helper.getPrintWord(10));
	   }
	
	@Test
	   public void returnsFizzBuzzWhenNumberIsMultipleOfThreeAndFive() throws Exception {
		assertEquals("buzz", helper.getPrintWord(10));
	}
}
