package test.java.com.aviva;

import java.text.ParseException;

import main.java.com.aviva.validator.InputFizzBuzzValidatorImpl;
import main.java.com.aviva.validator.interfaces.IInputValidator;

import org.junit.Assert;
import org.junit.Test;


public class FizzBuzzValidatorTest {

	IInputValidator inputValidator=new InputFizzBuzzValidatorImpl();
	
	@Test(expected=NumberFormatException.class)
	public final void throwsNumberFormatExceptionWhenNonNumberisPassed(){
			Assert.assertEquals("ParseException",inputValidator.isValidInput("NonNumber"));
	}
	
	@Test
	public final void returnsFalseWhenNegativeNumberIsUsed(){
		try {
			Assert.assertFalse("Negative input is not allowed.",inputValidator.isValidInput("-1"));
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}
	
	@Test
	public final void returnsTrueWhenPositiveNumberIsUsed(){
		try {
			Assert.assertTrue("Negative input is not allowed.",inputValidator.isValidInput("11"));
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}
	
	@Test 
	public final void returnFalseWhenInputNotBetweenOneAndThousand(){
		Assert.assertFalse("Enetr number between 1 and 1000.",inputValidator.isInputInValidRange("1001"));
	}
	
	@Test 
	public final void returnTrueWhenInputIsBetweenOneAndThousand(){
		Assert.assertTrue("Enetr number between 1 and 1000.",inputValidator.isInputInValidRange("191"));
	}
}
