package FizzBuzzSerices.com.aviva.services;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class LogInputService {

    @RequestMapping("/")
    String home() {
        return "Test service";
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(LogInputService.class, args);
    }

}